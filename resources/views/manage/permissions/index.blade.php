@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Manage Permissions</h5>
                        <a href="{{ route('permissions.create') }}" class="btn btn-sm btn-primary pull-right">Create Permission</a>
                </div>

                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{ $permission->id }}</td>
                                    <td>{{ $permission->name }}</td>
                                    <td>{{ $permission->display_name }}</td>
                                    <td>{{ $permission->description }}</td>
                                    <td>{{ $permission->created_at->toFormattedDateString() }}</td>
                                    <td>
                                        <a href="{{ route('permissions.show', $permission->id) }}" class="btn btn-sm btn-outline-info">R</a>
                                        <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-sm btn-outline-warning">U</a>
                                        
                                        {!! Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'DELETE', 'style' => 'display:inline-block;']) !!}
                                            <button class="btn btn-sm btn-outline-danger">D</button>
                                        {!! Form::close() !!}
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection