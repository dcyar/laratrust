@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Permission Details</h5>
                </div>

                <div class="card-body d-flex justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{ $permission->display_name }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $permission->name }}</h6>
                            <hr />
                            <p class="card-text">{{ $permission->description }}</p>
                            <hr />
                            <a href="{{ route('permissions.edit', $permission->id) }}" class="card-link">Edit Permission</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection