@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Role Details</h5>
                </div>

                <div class="card-body d-flex justify-content-center">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{ $role->display_name }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $role->name }}</h6>
                            <ul>
                                @foreach ($role->permissions as $permission)
                                    <li>{{ $permission->display_name }}</li>
                                @endforeach
                            </ul>
                            <a href="{{ route('roles.edit', $role->id) }}" class="card-link">Edit Role</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection