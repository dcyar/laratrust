@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Manage Roles</h5>
                        <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary pull-right">Create Role</a>
                </div>

                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>{{ $role->created_at->toFormattedDateString() }}</td>
                                    <td>
                                        <a href="{{ route('roles.show', $role->id) }}" class="btn btn-sm btn-outline-info">R</a>
                                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-sm btn-outline-warning">U</a>
                                        <a href="{{ route('roles.destroy', $role->id) }}" class="btn btn-sm btn-outline-danger">D</a>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection